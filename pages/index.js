import Head from 'next/head'
import Link from 'next/link'

export default function Home() {
    return (
        <div className="mx-auto bg-gradient-to-r from-green-400  to-blue-500 bg-auto h-screen flex items-center justify-center">
            <Head>
                <title>Kindspot</title>
            </Head>

            <div className="grid grid-flow-col grid-rows-2 grid-cols-4 gap-4 mx-auto">
                <div className="text-white text-5xl bg-gray-400 shadow-lg rounded-2xl p-4">
                    <Link href="/affirmation">Affirmation</Link>
                </div>
                <div className="text-white text-5xl">
                    <Link href="/advice">Advice</Link>
                </div>

                <div className="text-white text-5xl">Todo</div>
                <div className="text-white text-5xl">
                    <Link href="/meetup">Meetup</Link>
                </div>

                <div className="text-white text-5xl">
                    <Link href="/cookbook">Cookbook</Link>
                </div>
            </div>
        </div>
    )
}
