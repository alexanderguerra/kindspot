import { useState, useEffect } from 'react'
import Head from 'next/head'

export default function Advice() {
    const [advice, setAdvice] = useState('')

    useEffect(() => {
        const url = 'https://api.adviceslip.com/advice'
        const fetchData = async () => {
            try {
                const response = await fetch(url)
                const json = await response.json()
                console.log(json)
                setAdvice(json.slip.advice)
            } catch (error) {
                console.log('error', error)
            }
        }
        fetchData()
    }, [])

    return (
        <div className="mx-auto bg-gradient-to-r from-green-400  to-blue-500 bg-auto h-screen flex items-center justify-center">
            <Head>
                <title>Kindspot</title>
            </Head>
            <h1 className="text-5xl text-center text-white">{advice}</h1>
        </div>
    )
}
