function Spotify({ genres }) {
    console.log(genres)
    return (
        <div>
            <h1>Spotify</h1>
        </div>
    )
}

export async function getStaticProps() {
    const data = await fetch(
        'https://api.spotify.com/v1/recommendations/available-genre-seeds',
        {
            headers: {
                Authorization: `Bearer ${process.env.SPOTIFY_OAUTH_TOKEN}`,
            },
        },
    )
    console.log(data)
    return {
        props: {
            genres: ['test', 'test2'],
        },
    }
}
export default Spotify
