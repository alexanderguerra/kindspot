import Head from 'next/head'

export default function Affirmation() {
    const affirmations = [
        'Today, I am brimming with energy and overflowing with joy.',
        'I am superior to negative thoughts and low actions.',
        'I can easily change something if I want to.',
        'I am happy with where I am in my journey.',
        'I am the master of my mind, body, heart, and soul.',
        'As I take steps to improve my life, my self-esteem improves as well.',
        'Emotions come and get and I am ever in the flow.',
        'I am building a life of love peace and joy.',
        'I choose to be happy everyday.',
        'My actions show others what I value and care about them.',
        'I am listening with my heart.',
        'I am putting my life in order so that I may expand and grow.',
        'I can persevere when things get hard.',
        'Happy thoughts create happy feelings.',
        'I am worthy of feeling happy everyday.',
        'I am energetic, enthusiastic, confident, and happy.',
        'Wonderful things happen to kind people.',
        'The past is over.',
        'The life I have always dreamed of is created by my choice to be joyful now.',
        'I am able to love myself for who I am.',
        'I open myself to joy, and pleasure, and fun.',
        'I trust that I am on the right path.',
        'I am more than my body.',
        'I attract joy and joyful situations to me.',
        'I am stronger than my fears.',
        'I am creating a glorious future for myself.',
        'I am totally committed to making positive changes in my life.',
    ]

    return (
        <div className="mx-auto bg-gradient-to-r from-green-400  to-blue-500 bg-auto h-screen flex items-center justify-center">
            <Head>
                <title>Kindspot</title>
            </Head>
            <h1 className="text-5xl text-center text-white">
                {affirmations[1]}
            </h1>
        </div>
    )
}
