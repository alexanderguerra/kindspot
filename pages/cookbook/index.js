export default function Cookbook({ data }) {
    const recipes = data.recipes
    console.log(recipes)
    return (
        <div>
            <h1>Cookbook</h1>
        </div>
    )
}

export function getStaticProps() {
    return {
        props: {
            data: {
                recipes: [{ title: 'Arepa' }],
            },
        },
    }
}
