# Kindspot

Kindspot is a [Next.js](https://nextjs.org/) web application.

The ideas was to make a template that I could go from zero to deploy in a reasonable amount to time. I would call that half a night. I think that is a good goal for scaffolding a project. If you have an idea, you don't want to be spending multiple sessions of work just setting the project up. You want to be able to have atleast 2 hours to sit down and create.

I wanted to make something for my mind. I wanted to make a React app. I was going to try a meditation timer. But, I thought this would be a good quick project I could get out.

I am working on a mac. This gives you an affirmation with an attractive background.

What do I want todo in the future?
I want to style and make a button that displays a random affirmation.

1. Make Next.js app.
2. Make a github/gitlab repo.
   1. Setup repo
3. Push Next.js app to git service
4. Make a new Vercel app and connect it to the github service.
5. Setup DNS
6. Start coding

## Service for hosting

I am using [https://vercel.com/dashboard](Vercel) for hosting. This is really nice. I don't have to make a server on a Digital Ocean droplet. The idea is you have a project. You connect the project to a git repository. From here you can deploy the branch you want to Vercel and whenever you push to your repo you trigger a build. This is really cool.

What did I learn?
What did I use?

Hosting
I
Git
[https://kindspot.alexguerra.dev](Kindspot)
